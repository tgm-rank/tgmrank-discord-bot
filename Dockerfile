FROM node:12-alpine

WORKDIR tgmrank-frontend

COPY ./package.json ./package.json
COPY ./yarn.lock ./yarn.lock
RUN yarn install

ADD . ./

RUN yarn test

CMD NODE_ENV=production yarn run bot
