import { MessageEmbed } from 'discord.js';
import fetch from 'node-fetch';
import {
    enhanceGameModel,
    leaderboardPath,
    lookup,
    ModeTags,
} from '../utility/game-modes';
import { asOrdinal } from '../utility/string-utilities';

export function isPersonalBest(recentActivityEntry) {
    return (
        recentActivityEntry.score?.rank != null &&
        (recentActivityEntry.delta?.mode?.rank != null ||
            recentActivityEntry.previousScore == null)
    );
}

async function fetchJson(url) {
    const res = await fetch(url);
    return await res.json();
}

export default class TgmRankClient {
    static baseUrl() {
        return process.env.BASE_URL;
    }
    static externalUrl() {
        return process.env.EXTERNAL_URL;
    }

    static async enhancedRecentActivity(page, pageSize, extendedRankingModes) {
        const recentActivity = await this.recentActivity(page, pageSize);
        const extendedRecentActivity = await this.recentActivity(
            page,
            pageSize,
            extendedRankingModes,
        );

        return recentActivity.map((activity, index) => {
            const extendedActivity = extendedRecentActivity[index];

            if (activity.score.scoreId !== extendedActivity.score.scoreId) {
                throw 'Invalid recent activity alignment';
            }
            activity.extendedDelta = extendedActivity.delta;

            return activity;
        });
    }

    static async recentActivity(page, pageSize, modes) {
        const url = new URL('/v1/score/activity', this.baseUrl());
        url.searchParams.append('page', page);
        url.searchParams.append('pageSize', pageSize);
        if (modes) {
            const modeIds = modes.join(',');
            url.searchParams.append('modeIds', modeIds);
        }
        url.searchParams.append('scoreStatus', 'pending,verified,accepted');

        const response = await fetch(url.toString());
        return await response.json();
    }

    static async getAllGames() {
        const url = `${this.baseUrl()}/v1/game`;
        const gameResponse = await fetchJson(url);

        return enhanceGameModel(gameResponse);
    }

    static async gameRanking(gameId) {
        const url = `${this.baseUrl()}/v1/game/${gameId}/ranking`;
        return await fetchJson(url);
    }

    static async modeRanking(modeId, asOf) {
        const url = new URL(`/v1/mode/${modeId}/ranking`, this.baseUrl());

        if (asOf != null) {
            url.searchParams.append('asOf', asOf);
        }

        return await fetchJson(url.toString());
    }

    static async getPlayerByName(playerName) {
        const url = `${this.baseUrl()}/v1/player?playerName=${playerName}`;
        return await fetchJson(url);
    }

    static async getPlayerScoresByName(playerName) {
        const url = `${this.baseUrl()}/v1/player/scores?playerName=${playerName}`;
        return await fetchJson(url);
    }

    // Excludes only accepts a single value right now, "comment" with a lowercase c...
    static buildScoreString(score) {
        let makeScoreComponent = (label, value) => {
            if (value != null && value.toString()?.trim().length > 0) {
                return `**${label}**: ${value}`;
            }
        };

        let playtime = score.playtime
            ? score.playtime.slice(3).slice(0, -1)
            : null;

        let gradeString = makeScoreComponent(
            'Grade',
            score.grade?.gradeDisplay,
        );
        if (gradeString && score.grade.line) {
            gradeString = `${gradeString} (${score.grade.line})`;
        }

        let components = [
            gradeString,
            makeScoreComponent('Level', score.level),
            makeScoreComponent('Time', playtime),
            makeScoreComponent('Score', score.score),
        ];

        return components.filter(c => c != null).join('\n');
    }

    static async makeRecentActivityEmbed(games, recentActivity) {
        const { game: currentGame, mode: currentMode } = lookup(
            games,
            recentActivity.gameId,
            recentActivity.modeId,
        );

        const currentScore = recentActivity.score;
        const previousScore = recentActivity.previousScore;

        const embed = new MessageEmbed()
            .setTitle(
                `__**${currentScore.player.playerName}** did a thing in **${currentGame.shortName} ${currentMode.modeName}**!__`,
            )
            .setColor(0x6e5a7d)
            .setFooter('TGM Rank @ theabsolute.plus');

        if (currentMode.tags?.includes(ModeTags.Carnival)) {
            embed.setTitle(
                `__**${currentScore.player.playerName} has added to the ${currentMode.modeName} DEATH TOLL!**__`,
            );
            embed.setColor(0xfb3640);
            const carnivalRanking = await this.modeRanking(
                currentMode.modeId,
                currentScore.createdAt,
            );
            const totalBeforeCurrentEntry = carnivalRanking.reduce(
                (sum, item) => sum + item.level,
                0,
            );

            let offset;
            if (isPersonalBest(recentActivity)) {
                offset =
                    previousScore?.level != null
                        ? currentScore.level - previousScore.level
                        : currentScore.level;
            } else {
                offset = 0;
            }

            const total = totalBeforeCurrentEntry + offset;

            embed.setDescription(
                `** :skull_crossbones: DEATH TOLL :skull_crossbones: **\n** :fire: ${total} (+${offset}) :fire: **`,
            );
        }

        this.addPersonalBest(embed, currentScore, previousScore);
        this.addExternalScoreLink(embed, currentScore.scoreId);
        this.addProofField(embed, currentScore.proof);
        this.addProofThumbnail(embed, currentScore.proof);
        this.addLeaderboardDeltaField(
            embed,
            currentGame,
            currentMode,
            recentActivity,
        );

        return embed;
    }

    static addExternalScoreLink(embed, scoreId) {
        embed.setURL(this.makeExternalScoreLink(scoreId));
    }

    static makeExternalScoreLink(scoreId) {
        return `${this.externalUrl()}/score/${scoreId}`;
    }

    static addLeaderboardDeltaField(embed, game, mode, recentActivity) {
        let makeRankDiffComponent = (delta, link) => {
            if (delta == null) {
                return null;
            }

            const encodedLink = new URL(link).toString();

            if (delta.rank == null) {
                return `[Unranked](${encodedLink})`;
            }

            // HACK: If this score is new to the leaderboard, you won't have a previousRank, but the recent activity response will tell you how many players you passed
            const rankRaise =
                delta.previousRank != null
                    ? delta.previousRank - delta.rank
                    : delta.playersOffset.length;

            return `[${asOrdinal(delta.rank)}](${encodedLink}) (+${rankRaise})`;
        };

        embed.addField(
            '__**Overall***__',
            makeRankDiffComponent(
                recentActivity.delta.overall,
                `${this.externalUrl()}/overall`,
            ),
            true,
        );
        embed.addField(
            '__**Game***__',
            makeRankDiffComponent(
                recentActivity.delta.game,
                `${this.externalUrl()}${leaderboardPath({ game })}`,
            ),
            true,
        );
        embed.addField(
            '__**Mode**__',
            makeRankDiffComponent(
                recentActivity.delta.mode,
                `${this.externalUrl()}${leaderboardPath({ game, mode })}`,
            ),
            true,
        );
        embed.addField(
            '__**Overall+**__',
            makeRankDiffComponent(
                recentActivity.extendedDelta.overall,
                `${this.externalUrl()}/overall/extended`,
            ),
            true,
        );
        embed.addField(
            '__**Game+**__',
            makeRankDiffComponent(
                recentActivity.extendedDelta.game,
                `${this.externalUrl()}${leaderboardPath({
                    game,
                    isExtended: true,
                })}`,
            ),
            true,
        );
        embed.addField('\u200b', '\u200b', true);
    }

    static addProofThumbnail(embed, proofArray) {
        if (!embed) {
            return;
        }

        if (!proofArray || !proofArray[0]) {
            return;
        }

        let proof = proofArray[0];
        if (proof) {
            const proofType = proof.type.toLowerCase();
            if (proofType === 'video') {
                if (proof.link.match(/youtu/)) {
                    let re = /^(https?:\/\/)?((www\.)?(youtube(-nocookie)?|youtube.googleapis)\.com.*(v\/|v=|vi=|vi\/|e\/|embed\/|user\/.*\/u\/\d+\/)|youtu\.be\/)([_0-9a-z-]+)/i;
                    let id = proof.link.match(re)[7];

                    embed.setThumbnail(
                        `https://img.youtube.com/vi/${id}/1.jpg`,
                    );
                }
            } else if (proofType === 'image') {
                embed.setThumbnail(proof.link);
            }
        }
    }

    static addProofField(embed, proofArray) {
        if (!embed) {
            return;
        }

        if (!proofArray || !proofArray[0]) {
            return;
        }

        embed.addField('__**Proof**__', this.buildProofString(proofArray));
    }

    static buildProofString(proofArray) {
        return proofArray
            .map((p, i) => `[[#${i + 1}: ${p.type.toLowerCase()}]](${p.link})`)
            .join(' ');
    }

    static addPersonalBest(embed, score, previousScore) {
        embed.addField('__**Details**__', this.buildScoreString(score), true);

        if (previousScore != null) {
            embed.addField(
                '__**Previous PB**__',
                `${this.buildScoreString(previousScore)}`,
                true,
            );
        } else {
            embed.addField('\u200b', '\u200b', true);
        }
        embed.addField('\u200b', '\u200b', true);

        if (score.comment?.length > 0) {
            embed.addField('__**Comment**__', score.comment);
        }

        return embed;
    }
}
