import { isPersonalBest } from './tgmrank-client';

describe('isPersonalBest', () => {
    it('should return true for newly ranked scores', () => {
        const recentActivity = {
            score: {
                rank: 56,
            },
            previousScore: null,
            delta: {
                mode: {
                    rank: 56,
                },
            },
        };
        const result = isPersonalBest(recentActivity);
        expect(result).toBeTruthy();
    });

    it('should return true for PB that ranks better than old score', () => {
        const recentActivity = {
            score: {
                rank: 56,
            },
            previousScore: {
                rank: 58,
            },
            delta: {
                mode: {
                    rank: 56,
                    previousRank: 58,
                },
            },
        };
        const result = isPersonalBest(recentActivity);
        expect(result).toBeTruthy();
    });

    it('should return false for score that is not a PB', () => {
        const recentActivity = {
            score: {
                rank: null,
            },
            previousScore: null,
            delta: {
                mode: {
                    rank: null,
                    previousRank: null,
                },
            },
        };
        const result = isPersonalBest(recentActivity);
        expect(result).toBeFalsy();
    });
});
