import slugify from 'slugify';

export const ModeTags = {
    Main: 'MAIN',
    Extended: 'EXTENDED',
    Event: 'EVENT',
    Carnival: 'CARNIVAL',
};

export function leaderboardPath({ game, mode, isExtended } = {}) {
    if (game == null) {
        return '';
    }

    const preferredGameName = game.shortName.toLowerCase();

    if (mode == null) {
        return `/${preferredGameName}${isExtended ? '/extended' : ''}`;
    }

    const preferredModeName = (
        mode.aliases?.[0] ?? slugify(mode.modeName)
    ).toLowerCase();
    return `/${preferredGameName}/${preferredModeName}`;
}

export function lookupGame(games, gameLookupId) {
    if (games == null || gameLookupId == null) {
        return null;
    }

    if (Number.isInteger(gameLookupId)) {
        return games?.find(g => g.gameId === gameLookupId);
    }

    let gameLookupIdString = gameLookupId?.toLowerCase();
    return games?.find(
        g =>
            g.shortName.toLowerCase() === gameLookupIdString ||
            g.aliases?.includes(gameLookupIdString),
    );
}

export function lookupMode(game, modeLookupId) {
    if (game == null || modeLookupId == null) {
        return null;
    }

    if (Number.isInteger(modeLookupId)) {
        return game?.modes?.find(m => m.modeId === modeLookupId);
    }

    const modeLookupIdString = modeLookupId?.toLowerCase();

    return game?.modes?.find(
        m =>
            slugify(m.modeName.toLowerCase()) === slugify(modeLookupIdString) ||
            m.aliases?.includes(modeLookupIdString),
    );
}

export function lookup(games, gameLookupId, modeLookupId) {
    const game = lookupGame(games, gameLookupId);
    const mode = lookupMode(game, modeLookupId);
    return { game, mode };
}

function modeIsRecent(mode) {
    const now = Date.now();
    const oneWeekInMs = 604800000;
    if (mode.submissionRange != null) {
        const lowerBoundPass = mode.submissionRange.start
            ? now >= Date.parse(mode.submissionRange.start) - oneWeekInMs
            : true;

        const upperBoundPass = mode.submissionRange.end
            ? now <= Date.parse(mode.submissionRange.end) + oneWeekInMs
            : true;

        return lowerBoundPass && upperBoundPass;
    }
    return true;
}

function modeStarted(mode) {
    return mode.submissionRange != null && mode.submissionRange.start != null
        ? Date.now() >= Date.parse(mode.submissionRange.start)
        : true;
}

function modeIsActive(mode) {
    const now = Date.now();
    if (mode.submissionRange != null) {
        const lowerBoundPass = mode.submissionRange.start
            ? now >= Date.parse(mode.submissionRange.start)
            : true;

        const upperBoundPass = mode.submissionRange.end
            ? now <= Date.parse(mode.submissionRange.end)
            : true;

        return lowerBoundPass && upperBoundPass;
    }
    return true;
}

function addModeAliases(modeList, modeId, aliasList) {
    const mode = modeList.find(m => m.modeId === modeId);
    if (mode != null) {
        mode.aliases = aliasList;
    }
}

export function enhanceGameModel(games) {
    // This isn't completely awful, is it?
    games[1] = games.tgm1 = {
        ...games.find(g => g.gameId === 1),
        aliases: ['tgm1', 'tgm'],
        rankingModes: [1, 2],
        extendedRankingModes: [1, 2, 3, 4, 29],
        wikiLink: 'https://tetris.wiki/Tetris_The_Grand_Master',
        twitchLink:
            'https://www.twitch.tv/directory/game/Tetris%3A%20The%20Grand%20Master',
    };

    games[2] = games.tap = {
        ...games.find(g => g.gameId === 2),
        aliases: ['tgm2', 'tgm2p', 'tgm2+', 'tap'],
        rankingModes: [6, 7],
        extendedRankingModes: [12, 6, 7, 13, 11, 9, 14, 8],
        wikiLink: 'https://tetris.wiki/Tetris_The_Absolute_The_Grand_Master_2',
        twitchLink:
            'https://www.twitch.tv/directory/game/Tetris%20the%20Absolute%3A%20The%20Grand%20Master%202',
    };

    games[3] = games.ti = {
        ...games.find(g => g.gameId === 3),
        aliases: ['tgm3', 'ti'],
        rankingModes: [15, 16],
        extendedRankingModes: [15, 16, 17, 18, 25, 19, 30],
        wikiLink:
            'https://tetris.wiki/Tetris_The_Grand_Master_3_Terror-Instinct',
        twitchLink:
            'https://www.twitch.tv/directory/game/Tetris%3A%20The%20Grand%20Master%203%20-%20Terror%E2%80%91Instinct',
    };

    games[0] = games.overall = {
        ...games.find(g => g.gameId === 0),
        aliases: ['all', 'overall'],
        rankingModes: games.tgm1.rankingModes.concat(
            games.tap.rankingModes,
            games.ti.rankingModes,
        ),
        extendedRankingModes: games.tgm1.extendedRankingModes.concat(
            games.tap.extendedRankingModes,
            games.ti.extendedRankingModes,
        ),
    };

    for (let g = 0; g < games.length; g++) {
        for (let m = 0; m < games[g]?.modes?.length; m++) {
            games[g].modes[m].game = games[g];
            games[g].modes[m].isRanked = games[g].extendedRankingModes.includes(
                games[g].modes[m].modeId,
            );

            games[g].modes[m].isActive = modeIsActive(games[g].modes[m]);
            games[g].modes[m].hasStarted = modeStarted(games[g].modes[m]);
            games[g].modes[m].isRecent = modeIsRecent(games[g].modes[m]);
        }
    }

    games.tap.modes.find(m => m.modeId === 8).links = [
        {
            title: 'Death Series of 5 Calculator',
            link: '/tools/death',
        },
    ];

    games.tap.modes
        .filter(m => m?.tags?.includes(ModeTags.Carnival))
        .forEach(
            m =>
                (m.links = [
                    {
                        title: 'Carnival of Death (tetris.wiki)',
                        link: 'https://tetris.wiki/Carnival_of_Death',
                    },
                ]),
        );

    addModeAliases(games.tap.modes, 8, ['death-series', 'death-series-of-5']);
    addModeAliases(games.tap.modes, 13, ['tgm-plus']);

    addModeAliases(games.ti.modes, 15, ['master', 'master-classic']);
    addModeAliases(games.ti.modes, 30, [
        'qualified-master',
        'qualified-master-classic',
    ]);
    addModeAliases(games.ti.modes, 16, ['shirase', 'shirase-classic']);
    addModeAliases(games.ti.modes, 17, ['easy', 'easy-classic']);
    addModeAliases(games.ti.modes, 18, ['sakura', 'sakura-classic']);
    addModeAliases(games.ti.modes, 19, [
        'master-secret-grade',
        'master-secret-grade-classic',
    ]);
    addModeAliases(games.ti.modes, 27, [
        'shirase-secret-grade',
        'shirase-secret-grade-classic',
    ]);
    addModeAliases(games.ti.modes, 25, ['big', 'big-classic']);
    addModeAliases(games.ti.modes, 20, ['master-world']);
    addModeAliases(games.ti.modes, 31, ['qualified-master-world']);
    addModeAliases(games.ti.modes, 21, ['shirase-world']);
    addModeAliases(games.ti.modes, 22, ['easy-world']);
    addModeAliases(games.ti.modes, 23, ['sakura-world']);
    addModeAliases(games.ti.modes, 24, ['master-secret-grade-world']);
    addModeAliases(games.ti.modes, 28, ['shirase-secret-grade-world']);
    addModeAliases(games.ti.modes, 26, ['big-world']);

    return games;
}

export function isRequired(modeField) {
    return modeField === 'REQUIRED';
}

export function isOptional(modeField) {
    return modeField === 'OPTIONAL';
}

export function mustBeEmpty(modeField) {
    return modeField === 'HIDDEN';
}
