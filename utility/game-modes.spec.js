import { leaderboardPath, lookupGame, lookupMode } from './game-modes';

describe('game-mode utilities', () => {
    describe('leaderboardPath', () => {
        it('should return empty string when game is not given', () => {
            const path = leaderboardPath({});
            expect(path).toBe('');
        });

        it('should return path to game leaderboards', () => {
            const path = leaderboardPath({
                game: {
                    shortName: 'HELLO',
                },
            });

            expect(path).toBe('/hello');
        });

        it('should return path to extended game leaderboards', () => {
            const path = leaderboardPath({
                game: {
                    shortName: 'HELLO',
                },
                isExtended: true,
            });

            expect(path).toBe('/hello/extended');
        });

        it('should return preferred aliased path to mode leaderboard', () => {
            const path = leaderboardPath({
                game: {
                    shortName: 'HELLO',
                },
                mode: {
                    modeName: 'One Two Three',
                    aliases: ['WORLD', 'WORLD-2'],
                },
            });

            expect(path).toBe('/hello/world');
        });

        it('should return path with friendly version of mode name when mode has no aliases', () => {
            const path = leaderboardPath({
                game: {
                    shortName: 'HELLO',
                },
                mode: {
                    modeName: 'One Two Three',
                },
            });

            expect(path).toBe('/hello/one-two-three');
        });
    });

    describe('lookupGame', () => {
        const games = [
            {
                gameId: 1,
                shortName: 'One',
                aliases: ['uno', 'eins'],
            },
        ];

        // TODO: Do something specific when lookup is not an integer or string

        it('should return null when games is null', () => {
            const game = lookupGame(null, 1);
            expect(game).toBeNull();
        });

        it('should return null when lookup id is null', () => {
            const game = lookupGame([], null);
            expect(game).toBeNull();
        });

        it('should lookup game with gameId when lookup is an integer', () => {
            const game = lookupGame(games, 1);
            expect(game).toBe(games[0]);
        });

        it('should lookup game with name', () => {
            const game = lookupGame(games, 'one');
            expect(game).toBe(games[0]);
        });

        it('should lookup game with aliases', () => {
            for (const alias of games[0].aliases) {
                const game = lookupGame(games, alias);
                expect(game).toBe(games[0]);
            }
        });
    });

    describe('lookupMode', () => {
        const game = {
            modes: [
                {
                    modeId: 1,
                    modeName: 'Hello World',
                    aliases: ['bonjour', 'hola'],
                },
            ],
        };

        it('should return null when lookup id is null', () => {
            const mode = lookupMode(game, null);
            expect(mode).toBeNull();
        });

        it('should return null when game is null', () => {
            const mode = lookupMode(null, 1);
            expect(mode).toBeNull();
        });

        it('should lookup mode with modeId when lookup is an integer', () => {
            const mode = lookupMode(game, 1);
            expect(mode).toBe(game.modes[0]);
        });

        it('should lookup mode by name', () => {
            for (const modeLookup of ['Hello world', 'hello-world']) {
                const mode = lookupMode(game, modeLookup);
                expect(mode).toBe(game.modes[0]);
            }
        });

        it('should lookup mode by alias', () => {
            for (const alias of game.modes[0].aliases) {
                const mode = lookupMode(game, alias);
                expect(mode).toBe(game.modes[0]);
            }
        });
    });
});
