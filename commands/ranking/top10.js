import { MessageEmbed } from 'discord.js';
import Commando from 'discord.js-commando';
import TgmRankClient from '../../repository/tgmrank-client';
import {
    leaderboardPath,
    lookupGame,
    lookupMode,
} from '../../utility/game-modes';

module.exports = class Top10Command extends Commando.Command {
    constructor(client) {
        super(client, {
            name: 'top10',
            aliases: [],
            group: 'ranking',
            memberName: 'top10',
            description: 'Show the top 10 players for a game/mode',
            examples: [
                'top10',
                'top10 overall',
                'top10 tgm1',
                'top10 TGM2+',
                'top10 TAP',
                'top10 TGM3',
                'top10 TI',
                'top10 tgm1 secretgrade',
                'top10 tap "Death (Series of 5)"',
                'top10 ti masterc',
            ],
            args: [
                {
                    key: 'game',
                    label: 'game name',
                    prompt: 'What game would you like to get rankings for?',
                    type: 'string',
                    default: '',
                },
                {
                    key: 'mode',
                    label: 'mode name',
                    prompt: 'What mode would you like to get rankings for?',
                    type: 'string',
                    default: '',
                },
            ],
        });
    }

    async run(msg, args) {
        const removeDefaultValues = args => {
            Object.entries(args).forEach(([key, value]) => {
                if (!value) {
                    delete args[key];
                }
            });

            return args;
        };

        removeDefaultValues(args);

        const requestedGame = args['game'] ?? 'overall';
        const requestedMode = args['mode'];

        const games = await TgmRankClient.getAllGames();
        const game = lookupGame(games, requestedGame.toLowerCase());

        if (game == null) {
            msg.channel.send(`Invalid Game '${requestedGame}'.`);
            return;
        }

        let ranking;
        let rankingDescription;
        let externalUrl;

        if (!requestedMode) {
            ranking = await TgmRankClient.gameRanking(game.gameId);
            rankingDescription = game.shortName;
            externalUrl = `${process.env.EXTERNAL_URL}${leaderboardPath({
                game,
            })}`;
        } else {
            const mode = lookupMode(game, requestedMode);
            if (mode == null) {
                msg.channel.send(
                    `Invalid Mode '${requestedMode}' for game ${requestedGame}.`,
                );
                return;
            }

            ranking = await TgmRankClient.modeRanking(mode.modeId);
            rankingDescription = `${game.shortName} ${mode.modeName}`;
            externalUrl = `${process.env.EXTERNAL_URL}${leaderboardPath({
                game,
                mode,
            })}`;
        }

        const rankingString = ranking
            .filter(r => r.rank <= 10)
            .map(r => `${r.rank} - ${r.player.playerName}`)
            .join('\n');

        const embed = new MessageEmbed()
            .setURL(encodeURI(externalUrl))
            .setTitle(`__**Top 10 ${rankingDescription} Players**__`)
            .setColor(0x375a7d)
            .setDescription(rankingString)
            .setFooter('TGM Rank @ theabsolute.plus');

        await msg.channel.send({
            content: `Top 10 ${rankingDescription} Players`,
            embed,
        });
    }
};
