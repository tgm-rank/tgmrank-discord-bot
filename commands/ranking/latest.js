import Commando from 'discord.js-commando';
import TgmRankClient from '../../repository/tgmrank-client';

module.exports = class LatestScoreCommand extends Commando.Command {
    constructor(client) {
        super(client, {
            name: 'latest',
            aliases: ['latest'],
            group: 'ranking',
            memberName: 'latest',
            description: 'Get the most recently added score',
            examples: ['latest'],
        });
    }

    async run(msg) {
        const games = await TgmRankClient.getAllGames();
        const recentActivity = await TgmRankClient.enhancedRecentActivity(
            1,
            1,
            games.overall.extendedRankingModes,
        );

        const [mostRecentScore] = recentActivity;
        const embed = await TgmRankClient.makeRecentActivityEmbed(
            games,
            mostRecentScore,
        );

        // Send the embed to the same channel as the message
        await msg.channel.send({
            content: 'Latest score',
            embed,
        });
    }
};
