import { MessageEmbed } from 'discord.js';
import Commando from 'discord.js-commando';

import TgmRankClient from '../../repository/tgmrank-client';
import { lookupGame, lookupMode } from '../../utility/game-modes';

module.exports = class PersonalBestCommand extends Commando.Command {
    constructor(client) {
        super(client, {
            name: 'pb',
            aliases: ['ranking', 'player'],
            group: 'ranking',
            memberName: 'pb',
            description: 'Show player ranking or specific mode pbs',
            examples: [
                'pb mhl tgm1 20g',
                'pb xyrnq tap "Death (Series of 5)"',
                'pb edo ti shirasec',
            ],
            args: [
                {
                    key: 'player',
                    label: 'player name',
                    prompt: 'Which player would you like to know more about?',
                    type: 'string',
                    validator: p => p,
                },
                {
                    key: 'game',
                    label: 'game name',
                    prompt: 'What game would you like to get rankings for?',
                    type: 'string',
                    default: '',
                    validator: g => g,
                },
                {
                    key: 'mode',
                    label: 'mode name',
                    prompt: 'What mode would you like to get rankings for?',
                    type: 'string',
                    default: '',
                    validator: m => m,
                },
            ],
        });
    }

    async run(msg, args) {
        const requestedPlayer = args['player'];
        const requestedGame = args['game'];
        const requestedMode = args['mode'];

        const games = await TgmRankClient.getAllGames();
        const game = lookupGame(games, requestedGame);
        if (game == null) {
            msg.channel.send(`Invalid Game '${requestedGame}'.`);
            return;
        }

        const mode = lookupMode(game, requestedMode);
        if (mode == null) {
            msg.channel.send(
                `Invalid Mode '${requestedMode}' for game ${requestedGame}.`,
            );
            return;
        }

        const player = await TgmRankClient.getPlayerByName(requestedPlayer);
        if (player.error) {
            msg.channel.send(`Could not find player ${requestedPlayer}`);
            return;
        }

        const playerScores = await TgmRankClient.getPlayerScoresByName(
            requestedPlayer,
        );

        const bestScore = playerScores.find(
            s =>
                s.gameId === game.gameId &&
                s.modeId === mode.modeId &&
                s.status.toLowerCase() !== 'rejected',
        );
        if (!bestScore) {
            msg.channel.send(
                `${player.playerName} does not have a valid score for ${game.shortName} ${mode.modeName}`,
            );
            return;
        }

        const scoreDescription = TgmRankClient.buildScoreString(bestScore);

        const title = `${player.playerName}'s ${game.shortName} ${mode.modeName} Personal Best`;
        const embed = new MessageEmbed()
            .setTitle(`__**${title}**__`)
            .setColor(0x375a7d)
            .setDescription(scoreDescription)
            .setFooter('TGM Rank @ theabsolute.plus');

        TgmRankClient.addExternalScoreLink(embed, bestScore.scoreId);
        TgmRankClient.addProofThumbnail(embed, bestScore.proof);
        TgmRankClient.addProofField(embed, bestScore.proof);

        await msg.channel.send({
            content: title,
            embed,
        });
    }
};
