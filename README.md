# TGM Rank Discord Bot

This is it.

# Local Setup

Create a new discord application, instructions [here](https://github.com/reactiflux/discord-irc/wiki/Creating-a-discord-bot-&-getting-a-token).

Copy .env.default to .env and modify the variables accordingly.

```
BASE_URL=http://localhost
EXTERNAL_URL=https://theabsolute.plus:1330
TOKEN=MyCoolBotToken
LATEST_SCORE_REFRESH_RATE=0 * * * * *
LATEST_SCORE_WATCHED_CHANNELS=SomeChannelId
```

`TOKEN` should be your new discord bot token. The `Recent Activity` process will show scores in the channel pointed to by `LATEST_SCORE_WATCHED_CHANNELS`.

`LATEST_SCORE_REFRESH_RATE` is in a modified [cron](https://en.wikipedia.org/wiki/Cron) format that has with an extra value for seconds. The default refreshes every minute, but if you wanted to refresh every 30 seconds, it'd look like this: `*/30 * * * * *`

From here, you can use your own local installation or you can use docker.

## Local Node.js installation

1. Install node.js 11
2. Install [yarn](https://yarnpkg.com/)
3. Run `yarn run bot`

## Docker
1. Run `docker-compose up --build bot`