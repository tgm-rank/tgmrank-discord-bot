import 'core-js/stable';
import 'regenerator-runtime/runtime';

import dotenv from 'dotenv';
import Commando from 'discord.js-commando';
import path from 'path';
import { CronJob } from 'cron';

import TgmRankClient, { isPersonalBest } from './repository/tgmrank-client';

function takeWhile(fn, arr) {
    for (const [i, val] of arr.entries()) {
        if (!fn(val)) {
            return arr.slice(0, i);
        }
    }

    return arr;
}

dotenv.config();

const DISCORD_CLIENT_READY = 0;
let recentActivityJob;

const client = new Commando.Client({
    commandPrefix: '!',
    unknownCommandResponse: false,
});

client
    .on('error', console.error)
    .on('warn', console.warn)
    .on('debug', console.log)
    .on('ready', async () => {
        console.log(
            `Client ready; logged in as ${client.user.username}#${client.user.discriminator} (${client.user.id})`,
        );

        const watchedChannels = process.env.LATEST_SCORE_WATCHED_CHANNELS.split(
            ',',
        );

        let [mostRecentScore] = await TgmRankClient.enhancedRecentActivity(
            1,
            1,
        );
        let scoreIdsSeen = [mostRecentScore.score.scoreId];

        let recentActivityRefreshRate = process.env.LATEST_SCORE_REFRESH_RATE;
        if (recentActivityJob == null) {
            console.log(
                `Registering recent activity cron job (${recentActivityRefreshRate})`,
            );

            recentActivityJob = new CronJob(
                recentActivityRefreshRate,
                async () => {
                    const now = new Date().toISOString();

                    if (client.ws.status !== DISCORD_CLIENT_READY) {
                        console.log(
                            `Trying to check recent activity, but client is not ready: Status ${client.status} (${now})...`,
                        );
                    }

                    console.log(`Checking recent activity (${now})...`);

                    const games = await TgmRankClient.getAllGames();

                    // 3 is good enough
                    let recentActivity = await TgmRankClient.enhancedRecentActivity(
                        1,
                        3,
                        games.overall.extendedRankingModes,
                    );

                    recentActivity = takeWhile(
                        ra => !scoreIdsSeen.includes(ra.score.scoreId),
                        recentActivity,
                    );

                    recentActivity = recentActivity.reverse();

                    for (const ra of recentActivity) {
                        scoreIdsSeen.push(ra.score.scoreId);
                        if (!isPersonalBest(ra)) {
                            console.log(
                                `Skipping non-personal-best score: ${ra.score.scoreId}`,
                            );
                            return;
                        }

                        console.log(
                            `Sending recent activity message for scoreId: ${ra.score.scoreId}`,
                        );
                        const embed = await TgmRankClient.makeRecentActivityEmbed(
                            games,
                            ra,
                        );

                        for (const watchedChannelId of watchedChannels) {
                            const channel = client.channels.cache.get(
                                watchedChannelId,
                            );

                            if (channel != null) {
                                channel.send({
                                    content: 'Check this out!',
                                    embed,
                                });
                            }
                        }
                    }
                },
                () => {},
                true, // Start job now
            );
        }
    });

client.registry
    .registerGroup('ranking', 'Ranking')
    .registerDefaultGroups()
    .registerDefaultTypes()
    .registerDefaultCommands({
        help: true,
        ping: false,
        prefix: false,
        commandState: false,
        unknownCommand: false,
    })
    .registerCommandsIn(path.join(__dirname, 'commands'));

client.login(process.env.DISCORD_BOT_TOKEN).then().catch(console.error);
